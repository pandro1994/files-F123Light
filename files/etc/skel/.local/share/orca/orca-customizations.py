
# Start SimpleOrcaPluginLoader DO NOT TOUCH!
try:
  import importlib.util, os
  spec = importlib.util.spec_from_file_location('SimplePluginLoader', os.path.expanduser('~')+'/.config/SOPS/SimplePluginLoader.py')
  SimplePluginLoaderModule = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(SimplePluginLoaderModule)
except Exception as e:
  print('Problem while loading SOPS:' + str(e))
# End SimpleOrcaPluginLoader DO NOT TOUCH!

