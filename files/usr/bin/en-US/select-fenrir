#!/bin/bash
# select-fenrir.sh
# Description:
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=select-fenrir
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}

logFile="/var/log/${0##*/}"
echo -n | sudo tee "$logFile"

# Check for ~/git/fenrir
mkdir -p ~/git/
if [[ -d ~/git/fenrir ]]; then
	git -C ~/git/fenrir pull  |& log
else
	git clone https://github.com/chrys87/fenrir.git ~/git/fenrir |& log
fi
# Select a branch
if [[ -n "$1" ]]; then
	branch="$1"
else
	branches="$(git -C ~/git/fenrir branch -a)"
	branches="${branches//\* }"
	declare -a branchList=(stable stable)
	for i in $branches ; do
		branchList+=(${i##*/} ${i##*/})
	done
	branch=$(menulist ${branchList[@]})
fi

# If there is no branch, user canceled from menu.
[[ -z "$branch" ]] && exit 0

# Make sure that we're using the correct branch and that it exists.
if [[ "$branch" != "stable" ]]; then
	git -C ~/git/fenrir checkout -q $branch 2> /dev/null || {
		echo "The given branch, '$branch', does not exist." | tee -a "$logFile";
		exit 1;
	}
	# Make sure the branch is up to date
	git -C ~/git/fenrir pull |& log
fi

# check for running fenrir service
systemctl is-active fenrirscreenreader |& log && sudo systemctl -q stop fenrirscreenreader

# Stop a running fenrir not controled by systemd.
echo "command quitapplication" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock 2> /dev/null

# One last attempt to kill any running fenrir applications.
sudo killall -15 fenrir |& log

# Remove the PID file if it still exists
sudo rm -vf /run/fenrir.pid |& log

# Start the stable version if requested
if [[ "$branch" == "stable" ]]; then
	sudo systemctl start fenrirscreenreader |& log
	exit 0
fi

# Start the git version of Fenrir
sudo ~/git/fenrir/src/fenrir-daemon |& log

# Wait up to 5 seconds for a new fenrir pid
i=0
sleep 1
while [[ $? -ne 0 || $i -lt 10 ]]; do
	sleep 0.5
	((i++))
# Check that it is working, restart if not.
echo "command say $(gettext "You are now running fenrir from the") $branch $(gettext "branch. If you can hear this, please press enter. If you can not, the computer will restart and use the installed copy of Fenrir.")" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock &> /dev/null && break
done |& log

read -t 30 || reboot || sudo reboot

exit 0
