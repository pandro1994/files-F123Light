#!/bin/bash
# preferences-manager.sh
# Description: functions to write the ~/.preferences file and keep it readable.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
write_preferences() {
    if [[ -n "${preferences[*]}" ]]; then
        echo 'declare -Ag preferences=(' > ~/.preferences
        for i in ${!preferences[@]} ; do
            if [[ "${preferences[$i]}" =~ ^[0-9]+ ]]; then
                echo "[${i}]=${preferences[$i]}" >> ~/.preferences
            else
                echo "[${i}]=\"${preferences[$i]}\"" >> ~/.preferences
            fi
        done
        echo ')' >> ~/.preferences
    fi
}
