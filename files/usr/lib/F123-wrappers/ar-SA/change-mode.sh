#!/bin/bash
# change-mode: Configuration utility to change the menu and available services.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

export TEXTDOMAIN=change-mode
export TEXTDOMAINDIR=/usr/share/locale
. gettext.sh

# Load F123 includes
for i in /usr/lib/F123-includes/*.sh ; do
    source $i
done

if [[ "$(whoami)" != "root" ]]; then
    msgbox "Please run this script as root."
    exit 0
fi

# If $1 exists check for it and set mode if it exists.
if [[ -n "$1" ]]; then
    if [[ -x "/usr/lib/F123-wrappers/pdmenu_$1.sh" ]]; then
sed -i "s#preproc:/usr/lib/F123-wrappers/pdmenu_.*\.sh#preproc:/usr/lib/F123-wrappers/pdmenu_${1}.sh#" /etc/pdmenurc
        echo -n "$1" | sudo tee /etc/mode &> /dev/null
    else
        echo "menu mode $1 not found. Please make sure the file exists and is executable."
        exit 1
    fi
    exit 0
fi


# Create an array of menus.
declare -a modeList
for i in /usr/lib/F123-wrappers/pdmenu_*.sh ; do
    i="${i##*_}"
    i="${i%.sh}"
   modeList+=($i)
done
mode="$(menulist $(for i in ${modeList[@]} Cancel ; do echo "$i $i";done))"
# Check for cancelation conditions.
if [[ "$mode" == "Cancel" || -z "$mode" ]]; then
    exit 0
fi

sed -i "s#preproc:/usr/lib/F123-wrappers/pdmenu_.*\.sh#preproc:/usr/lib/F123-wrappers/pdmenu_${mode}.sh#" /etc/pdmenurc

exit 0
