#!/bin/bash
# chuser.sh
# Description: Changes the username to something new at login
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=chuser
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

# The user can not be logged in when the name change occurs.
# Write a file to /etc/cron.d/chuser
# The file will run at boot, change the username, and delete itself.

if [[ "$(whoami)" == "root" ]]; then
	gettext -e "Please run this script as the user you would like to rename, not as root.\n"
	echo "Can not rename root user." | log
	exit 1
fi

oldUser="$USER"
echo "Current username is $oldUser" | log

groups="$(groups "$oldUser")"
groups="${groups// /,}"
echo "Groups to migrate with the new user are: $groups" | log

newUser="$1"
echo "New user is $newUser" | log
if ! [[ "$newUser" =~ ^[a-z][-a-z0-9]*$ ]]; then
	echo "Username $newUser failed validation." | log
	exit 1
fi

echo "Username $newUser passed validation." | log
echo "Writing /etc/cron.d/0chuser" | log

cat << EOF | sudo tee /etc/cron.d/0chuser &> /dev/null
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
@reboot root usermod -a -G $groups -m -d /home/$newUser -l $newUser $oldUser && sed -E -i "s/autologin +[a-zA-Z0-9-]+/autologin $newUser/g" /etc/systemd/system/getty@tty*.service.d/override.conf; rm -f /etc/cron.d/0chuser;reboot

EOF

# Files in cron.d must be 644 to work.
sudo chmod 644 /etc/cron.d/0chuser && echo "Setting file permissions on /etc/cron.d/0chuser to 644" | log

# Reboot the computer so the script can run
sudo reboot

exit 0
