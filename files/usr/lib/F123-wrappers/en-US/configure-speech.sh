#!/bin/bash
# configure-speech
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Storm Dragon, <storm_dragon@linux-a11y.org>
# Copyright 2018, Kyle, <kyle@free2.ml>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

export TEXTDOMAIN=configure-speech
export TEXTDOMAINDIR=/usr/share/locale
. gettext.sh

# include script functions
for i in /usr/lib/F123-includes/*.sh ; do
    source $i
done

# Log writing function
log() {
    # Usage: command | log for just stdout.
    # Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
                                                                                                                                                                
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

# For additional speech options, add them to the synthArray variable, then add the available languages
# to a line in the case below matching the option
# Note that espeak-ng supports the most languages, so other case lines
# can start with espeak-ng and remove unsupported languages

synthArray=(
    espeak-ng
    mbrola
    rhvoice
)

for i in ${synthArray[@]} ; do
    case $i in
        espeak-ng) languages=('af_ZA' 'ar_EG' 'de_DE' 'en_US' 'es_ES' 'fr_FR' 'hi_IN' 'hu_HU' 'id_ID' 'pl_PL' 'pt_BR' 'sw_TZ' 'tr_TR' 'vi_VN' 'zh_CN');;
        mbrola) languages=('af_ZA' 'ar_EG' 'de_DE' 'en_US' 'es_ES' 'fr_FR' 'hi_IN' 'hu_HU' 'id_ID' 'pl_PL' 'pt_BR' 'tr_TR' 'zh_CN');;
        rhvoice) languages=('en_US');;
    esac

    # Only add a speech provider option if it has at least one voice to speak the current language
    for l in ${languages[@]}; do
        if [[ "$l" == "${LANG::5}" ]]; then
            # Dialog requires 2 options for the menu, we hide the tags, but it still needs to be sent twice.
            speechOptions+=("$i" "$i")
        fi
    done
done

speechProvider="$(menulist ${speechOptions[@]})"
speechProvider="${speechProvider,,}"

case "$speechProvider" in
    "mbrola") speechProvider="espeak-ng-mbrola-generic";;
esac

# Exit if speechProvider remains unset, i.e.
# if the user has pressed the escape key to close the menu
test -z $speechProvider && exit 0

# Set the  chosen speech provider option.
sudo sed -i.bak "s/^[[:space:]]*DefaultModule [[:space:]]*\S*$/ DefaultModule   $speechProvider/" /etc/speech-dispatcher/speechd.conf |& log

# For espeak-ng to use the correct voice on US English based systems, it needs to be set to default language en-us.
# For pretty much everything else though, it has to just be en.
# So, check and set that here.
case "${LANG::5}" in
"en_US")
if [[ "${speechProvider::9}" == "espeak-ng" ]]; then
spdLang=en-us
else
spdLang=en
fi
sudo sed -i "s/^[[:space:]]*DefaultLanguage [[:space:]]*\S*$/ DefaultLanguage   $spdLang/" /etc/speech-dispatcher/speechd.conf |& log
;;
esac

# Clear any keypresses in the buffer:
read -t .001 continue

# Load the new settings:
sudo pkill -1 speech-dispatch |& log
spd-say -o $speechProvider -l $spdLang "$(gettext "If you can hear this press the 'Enter' key to try the new voice. If you do nothing, the voice will not be changed and you will be back to the menu in 10 seconds.")"
read -n1 -t10 continue
# Error code 142 means a key was not pressed, so restore from backup.
if [[ $? -ne 0 ]]; then
    sudo mv /etc/speech-dispatcher/speechd.conf.bak /etc/speech-dispatcher/speechd.conf |& log
    # Load the old settings:
    sudo pkill -1 speech-dispatch |& log
    exit 1
fi

# Restart Fenrir with new speech provider changes
clear
sudo systemctl restart fenrirscreenreader |& log
# Attempt to restart orca to apply changes if it is running:
if pgrep orca &> /dev/null ; then
    for i in {0..11} ; do
        DISPLAY=:$i orca --replace &
        [[ $? -eq 0 ]] && break
    done
fi

# Remove the backup file.
[[ -f /etc/speech-dispatcher/speechd.conf.bak ]] && sudo rm -f /etc/speech-dispatcher/speechd.conf.bak |& log

# Update the speech-dispatcher backup for restore accessibility.
sudo cp -v /etc/speech-dispatcher/speechd.conf  /etc/F123-Config/backup/speechd.conf |& log
exit 0
