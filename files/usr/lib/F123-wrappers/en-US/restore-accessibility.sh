#!/bin/bash
# reset-a11y.sh
# Description: completely restore the system to last known working state
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=reset-a11y
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

set -o pipefail

# Attempt to restore speech-dispatcher.
if [[ -r /etc/F123-Config/backup/speechd.conf ]]; then
	sudo mv -f /etc/F123-Config/backup/speechd.conf /etc/speech-dispatcher/speechd.conf | log
	sudo pkill -9 speech-dispatch
fi

# Attempt to restore Fenrir
if [[ -r /etc/F123-Config/backup/fenrir.conf ]]; then
	mv -f /etc/F123-Config/backup/fenrir.conf /etc/fenrirscreenreader/settings/settings.conf | log
	# Try to restart the installed copy of Fenrir.
	# check for running fenrir service
	systemctl is-active fenrirscreenreader |& log && sudo systemctl stop fenrirscreenreader |& log
                                                                                                                                                                
	# Stop a running fenrir not controled by systemd.
	echo "command quitapplication" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock |& log
                                                                                                                                                                
	# One last attempt to kill any running fenrir applications.
	sudo killall -15 fenrir |& log

	# Start installed copy
	sudo systemctl start fenrirscreenreader
fi

exit 0
