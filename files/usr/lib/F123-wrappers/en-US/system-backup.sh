#!/bin/bash
# system-backup.sh
# Description:
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=system-backup
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

# Prompt for backup media to be connnected.
msgbox "$(gettext "If you have not yet done so, please connect an sdcard to your computer. when you are ready to continue, press enter.")"

declare -a drivesMenu
for i in $(list_drives | grep 'sd[a-z]'); do
	drivesMenu+=($i $i)
done
backupDrive="$(menulist ${drivesMenu[@]})"

if [[ -z "$backupDrive" ]]; then
	echo "No drive selected, user canceled." | log
	exit 0
fi

# Try to unmount the selected drive
sudo umount -A -R -f -l ${backupDrive%:*} |& log

# Correct backupDrive so that we can use it for rpi-clone
backupDrive="${backupDrive##*/}"
backupDrive="${backupDrive%:*}"
backupDrive="${backupDrive//[[:digit:]]/}"
echo "Backup destination is $backupDrive" | log
# Try to unmount any remaining partitions on the selected drive
sudo umount -A -R -f -l /dev/${backupDrive}* |& log

sudo rpi-clone -U -v $backupDrive |& log | dialog --progressbox "Creating backup Image..." 0 0

exit 0
