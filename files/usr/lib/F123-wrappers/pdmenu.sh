#!/bin/bash
# F123 menu
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Kyle, <kyle@free2.ml>
# Copyright 2018, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

# Remember to make this script executable so pdmenu can access it.
# Remember for subshells and variables that need to be sent instead of used put a backslash before the dollar sign
export TEXTDOMAIN=pdmenu.sh
export TEXTDOMAINDIR=/usr/share/locale
. gettext.sh

# Comment modes here so they are easy to track. 9 is everything. Other modes should be below that number.
# Mode 0 default
# Mode 1 education
# Mode 8 expert
# Mode 9 development
mode="$(cat /etc/mode)"
mode="${mode:-0}"

show_menu() {
    # args: $1, Menu creation arguments, seed man pdmenurc for details
    # args: $2, Mode optional, defaults to 0
    local entryMode="${2:-0}"
    [[ $entryMode -gt $mode ]] && return
    echo "show:$1"
}

menu_entry() {
    # Optional directives should preceed menu commands.
    # GUI: program is a graphical application.
    # Arguments: $1 "menu entry".
    # Arguments: $2 "application name, e.g. "firefox".
    # Arguments: $3  mode, optional unless menu commands are given.
    # Arguments: $4  menu commands, optional, e.g. edit. Separate multiple directives with a comma.
    local gui=0
    case "$1" in
        "GUI")
            gui=1
            shift;;
    esac
    local entryMode="${3:-0}"
    [[ $entryMode -gt $mode ]] && return
    echo "group:$1"
    echo exec:::clear
    echo "exec:::/usr/lib/F123-wrappers/tips.sh $2"
    if [[ $gui -eq 1 ]]; then
        local fileName="$(ls /tmp/fenrirscreenreader-[0-9]*.sock 2> /dev/null | head -1)"
        if [[ -n "$fileName" ]]; then
            echo "exec:::echo \"command tempdisablespeech\" | socat - UNIX-CLIENT:$fileName"
        fi
        echo "exec::$4:ratpoison -c \"execa $2\""
    else
        echo "exec::$4:command $2"
    fi
    echo "endgroup"
}

book_reader() {
cat << endOfFunction
    group:$(gettext "Book Reader")
    exec::makemenu: \
    echo "menu:books:$(gettext "Books"):$(gettext "Select book to read")"; \
    find \$HOME -type f \( -iname "*.epub" -o -iname "*.pdf" \) -print0 |\
    while read -d \$'\0' i ; do \
        j="\$(basename "\$i")"; \
        echo "exec:_\${j%%.*}::/usr/lib/F123-wrappers/bookreader.sh '\$i'"; \
    done; \
    echo nop; \
    echo "exit:$(gettext "Media Menu")"
    show:::books
		remove:::books
	endgroup
endOfFunction
}

external_drives() {
# Only for modes 1 and above.
[[ $mode -lt 1 ]] && return
cat << endOfFunction
group:$(gettext "Manage External Drives")
    exec::makemenu: \
    echo "menu:external:$(gettext "External"):$(gettext "Select Drive")"; \
    rmdir /media/* &> /dev/null; \
    c=0; \
    for i in \$(find /media -maxdepth 1 ! -path /media -type d 2> /dev/null) ; do \
        ((c++)); \
        j="\${i/\/media\//}"; \
        echo "exec:_\$j::dragonfm '\$i'"; \
        echo "exec:$(gettext "Safely remove") _\$j::umount '\$i' 2> /dev/null || sudo umount '\$i'"; \
    done; \
    [[ \$c -gt 0 ]] || echo "exec:\$(gettext "No external drives found")::clear"; \
    echo "exit:$(gettext "Main Menu")";
    show:::external
    remove:::external
endgroup
endOfFunction
}

pandora() {
cat << endOfFunction
group:$(gettext "Pandora Internet Radio")
    exec:::/usr/lib/F123-wrappers/tips.sh pianobar
    exec:::test -d "${XDG_CONFIG_HOME:-$HOME/.config}/pianobar" || command /usr/lib/F123-wrappers/configure-pianobar.sh
    exec:::pianobar
endgroup
endOfFunction
}

cat << EOF
title:$(gettext "Welcome to F123Light")

# Define the main menu.
menu:main:$(gettext "F123 Light Main Menu"):$(gettext "Use the up and down arrow keys to select your choice and press the 'Enter' key to activate it.")
	$(show_menu "$(gettext "Games Menu")::games" 2)
	$(show_menu "$(gettext "Internet Menu"):$(gettext "Browser, e-mail and chat applications"):internet")
	$(show_menu "$(gettext "Media Menu"):$(gettext "Book reading, music and video applications"):media" 8)
	$(show_menu "$(gettext "Office Menu"):$(gettext "text, calendar and spreadsheet applications"):office")
	$(menu_entry "$(gettext "File Manager")" "dragonfm")
	# External drives are managed in a special menu created in a function.
$(external_drives)
$([[ -r ~/.config/F123/menu ]] && echo "show:$(gettext "Custom Accessories Menu"):$(gettext "User Defined Applications"):custom_accessories")
	nop
	$(show_menu "$(gettext "Settings Menu"):$(gettext "Configure this computer"):settings")
	$(show_menu "$(gettext "Help Menu"):$(gettext "Get Help with F123Light"):help")
	$(show_menu "$(gettext "Turn Off or Restart Computer")::power")
	nop
	# Exit to command line is a special case, not handled by the menu_entry function.
	$([[ $mode -ge 0 ]] && echo "exit:$(gettext "Exit to Command Line")")

# Submenu for games.
menu:games:$(gettext "Games")):
	$(menu_entry "$(gettext "Adventure")" "adventure" 8 "pause")
	$(menu_entry "$(gettext "Alter Aeon")" "/usr/lib/F123-wrappers/mud-loader.sh https://gitlab.com/stormdragon2976/tintin-alteraeon.git" 8)
	$(menu_entry "$(gettext "Arithmetic Challenge!")" "arithmetic" 8 "pause")
	$(menu_entry "$(gettext "Air Traffic Controler (Not screen reader friendly)")" "atc" 9 "pause")
	$(menu_entry "$(gettext "Backgammon (Not screen reader friendly)")" "backgammon" 9 "pause")
	$(menu_entry "$(gettext "Battlestar")" "battlestar" 8 "pause")
	$(menu_entry "$(gettext "Boggle (Not screen reader friendly)")" "boggle" 9 "pause")
	$(menu_entry "$(gettext "Canfield (Not screen reader friendly)")" "canfield" 9 "pause")
	$(menu_entry "$(gettext "Cribbage (Not screen reader friendly)")" "cribbage" 9 "pause")
	$(menu_entry "$(gettext "Go Fish")" "go-fish" 8 "pause")
	$(menu_entry "$(gettext "Gomoku")" "gomoku" 8 "pause")
	$(menu_entry "$(gettext "Hangman")" "hangman" 8 "pause")
	$(menu_entry "$(gettext "Horseshoes")" "horseshoes" 8)
	$(menu_entry "$(gettext "Hunt (Not screen reader friendly)")" "hunt" 9 "pause")
	$(menu_entry "$(gettext "Legends of Kallisti")" "/usr/lib/F123-wrappers/mud-loader.sh https://gitlab.com/hjozwiak/tintin-kallisti-pack.git" 8)
	$(menu_entry "$(gettext "Mille Bornes")" "mille" 9 "pause")
	$(menu_entry "$(gettext "Number")" "number" 8 "pause")
	$(menu_entry "$(gettext "Phantasia")" "phantasia" 8 "pause")
	$(menu_entry "$(gettext "Phase of the Moon")" "pom" 8 "pause")
	$(menu_entry "$(gettext "Primes")" "primes" 8 "pause")
	$(menu_entry "$(gettext "Robots (Not screen reader friendly)")" "robots" 9 "pause")
	$(menu_entry "$(gettext "Sail")" "sail" 8 "pause")
	$(menu_entry "$(gettext "Snake (Not screen reader friendly)")" "snake" 9 "pause")
	$(menu_entry "$(gettext "Tetris (Not screen reader friendly)")" "tetris-bsd" 9 "pause")
	$(menu_entry "$(gettext "Trek")" "trek" 8 "pause")
	$(menu_entry "$(gettext "Tux Math")" "tuxmath --tts" 1 "pause")
	$(menu_entry "$(gettext "Tux Type")" "tuxtype --tts" 1 "pause")
	$(menu_entry "$(gettext "Worm (Not screen reader friendly)")" "worm" 9 "pause")
	$(menu_entry "$(gettext "Wumpus")" "wump" 8 "pause")
	nop
	exit:$(gettext "Main Menu")

# submenu for internet applications.
menu:internet:$(gettext "Internet"):$(gettext "Internet programs")
	$(menu_entry "$(gettext "E-mail")" "/usr/lib/F123-wrappers/mail-launcher.sh")
	nop:$(gettext "Web Browsers")
	$(menu_entry "$(gettext "Basic Web Browser (W3M)")" "w3m" 8)
	$(menu_entry "GUI" "$(gettext "Full Web Browser (Firefox)")" "firefox")
	nop:$(gettext "Communication")
	$(menu_entry "$(gettext "Telegram")" "telegram-cli" 9)
	$(menu_entry "GUI" "$(gettext "Text Chat (Pidgin)")" "pidgin" 8)
	$(menu_entry "$(gettext "Voice Chat (Mumble)")" "barnard-ui")
	nop
	exit:$(gettext "Main Menu")

menu:media:$(gettext "Media"):$(gettext "Multi-media applications")
	$(menu_entry "$(gettext "CD Audio Ripper")" "ripit")
	$(menu_entry "$(gettext "Music Player")" "audacious -Hp ~/Music")
	$(menu_entry "$(gettext "Stringed Instrument Tuner")" "bashtuner")
	# Pandora is a special menu, so handled in its own function.
	$(pandora)
	$(menu_entry "$(gettext "Youtube (Audio Only)")" "youtube-viewer -novideo")
	$(menu_entry "$(gettext "Youtube (Full Video)")" "youtube-viewer")
	nop:$(gettext "Book Readers")
	# Book reader is a special menu handled in a function.
	$(book_reader)
	nop
	exit:$(gettext "Main Menu")

menu:office:$(gettext "Office"):$(gettext "Word processing, calendar, etc")
	$(menu_entry "$(gettext "Month Calendar")" "ncal" 8 "pause")
	$(menu_entry "$(gettext "Year Calendar")" "ncal -y" 8 "pause")
	$(menu_entry "$(gettext "Spreadsheet")" "sc-im" 8)
	$(menu_entry "$(gettext "Text Editor")" "ne --macro clear")
	$(menu_entry "GUI" "$(gettext "Graphical Text editor")" "pluma")
	$(menu_entry "$(gettext "Word Processor")" "wordgrinder" 9)
    nop:$(gettext "OCR")
    $(menu_entry "$(gettext "Lios OCR")" "lios" 9)
	nop:$(gettext "Office Suite")
	$(menu_entry "$(gettext "Libre Office Spreadsheet")" "localc" 8)
	$(menu_entry "$(gettext "Libre Office Word Processor")" "lowriter" 8)
	$(menu_entry "$(gettext "Libre Office Launcher")" "soffice" 8)
	nop
	exit:$(gettext "Main Menu")

# submenu for configuring the computer.
menu:settings:$(gettext "Settings"):$(gettext "System configuration")
    show:$(gettext "System Backup Menu")::backup
	$(menu_entry "$(gettext "Check for System Updates")" "update-f123light" 0 "pause")
	$(menu_entry "$(gettext "Change Passwords")" "/usr/lib/F123-wrappers/configure-passwords.sh")
	$(menu_entry "$(gettext "E-mail Configuration")" "configure-email")
	$(menu_entry "$(gettext "Security Configuration")" "/usr/lib/F123-wrappers/configure-security.sh")
	$(menu_entry "$(gettext "Configure Fast Language Switching")" "/usr/lib/F123-wrappers/language-chooser.sh")
	$(menu_entry "$(gettext "Change System Speech")" "/usr/lib/F123-wrappers/configure-speech.sh")
	$(menu_entry "$(gettext "Bluetooth manager")" "/usr/lib/F123-wrappers/blueman-launcher")
	$(menu_entry "$(gettext "Configure Wifi")" "configure-wifi")
	nop
	exit:$(gettext "Main Menu")

menu:backup:$(gettext "Backup or restore your computer"):$(gettext "Backup or restore your computer")
    $(menu_entry "$(gettext "Backup your data and settings")" "/usr/lib/F123-wrappers/backup-manager.sh")
    $(menu_entry "$(gettext "Restore your data and settings")" "/usr/lib/F123-wrappers/restore-manager.sh")
    $(menu_entry "$(gettext "Full system backup")" "/usr/lib/F123-wrappers/system-backup.sh")
    nop
    exit:$(gettext "Settings Menu")
 
menu:help:$(gettext "Get Help with F123 Light"):$(gettext "Get Help with F123Light")
	$(menu_entry "$(gettext "Get Help")" "gettext -e 'For help please subscribe to the F123 visual email list\nhttps://groups.io/g/F123-Visual-English'" 0 "pause")
    $(menu_entry "$(gettext "Access Text Chat to Request Help")" "irssi --home /etc/F123-Config/irssi")
    $(menu_entry "$(gettext "Request remote assistance with my computer")" "sudo cfh" 0 "pause")
	nop
	$(menu_entry "$(gettext "About F123Light")" "/usr/lib/F123-wrappers/about.sh" 0 "pause")
	nop
	exit:$(gettext "_Main Menu")

menu:power:$(gettext "Turn off or Restart Computer"):$(gettext "Shutdown or restart your computer")
	$(menu_entry "$(gettext "Turn Off")" "poweroff 2> /dev/null || sudo poweroff")
	$(menu_entry "$(gettext "Restart")" "reboot 2> /dev/null || sudo reboot")
	nop
	exit:$(gettext "Main Menu")

$(if [[ -r ~/.config/F123/menu ]]; then
    echo "menu:custom_accessories:$(gettext "User Defined Applications"):$(gettext "User Defined Applications")"
    cat ~/.config/F123/menu
    echo "nop"
    echo "exit:$(gettext "Main Menu (_M)")"
fi)
EOF
