#!/bin/bash
# about.sh
# Description: Provides nicely formatted version information.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Multilanguage support.
export TEXTDOMAIN=about.sh
export TEXTDOMAINDIR=/usr/share/locale
. gettext.sh

[[ -r "/etc/timestamp-f123light" ]] && version="$(</etc/timestamp-f123light)"
version="${version:-0000000000}"
version="${version::2}.${version:2:2}.${version:4:2}.${version:6:2}.${version:8:2}"

gettext -e "F123Light Copyright F123 Consulting\n\nVersion: "
echo "$version"
 
exit 0
