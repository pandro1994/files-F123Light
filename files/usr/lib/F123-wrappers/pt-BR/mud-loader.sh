#!/bin/bash
#!/bin/bash
# mud_loader.xh
# Description: Easlily load mud soundpacks.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=tmp
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "" &> /dev/null



mudURL="$1"
mudName="${mudURL##*/}"
mudName="${mudName%.git}"
if [[ ! -d "$HOME/$mudName" ]]; then 
    git -C "$HOME" clone "$mudURL" &> /dev/null | dialog --progressbox "Downloading and installing game files, please wait..." 0 0
else
    git -C "$HOME/$mudName" pull &> /dev/null | dialog --progressbox "Making sure everything is up to date, please wait..." 0 0
fi

cd "$HOME/$mudName"
mudLauncher="$(ls -1 *.tin)"
tt++ $mudLauncher
exit 0
