#!/bin/bash
# mumble-manager
# Description: Make managing servers with barnard easy.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=mumble-manager-add-server
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

[[ -d ~/.config/barnard ]] || mkdir ~/.config/barnard
if [[ ! -r ~/.config/barnard/servers.conf ]]; then
    echo "Adding default mumble server." | log
    echo "declare -Ag mumbleServerList=(" >  ~/.config/barnard/servers.conf
    echo "[F123Light_Mumble_server]=\"mumble.f123.org:64738\"" >> ~/.config/barnard/servers.conf
    echo ")" >> ~/.config/barnard/servers.conf
fi
source ~/.config/barnard/servers.conf

function add-server() {
    local serverName="$(inputbox "$(gettext "Enter a name for the new server:")")"
    [[ $? -ne 0 ]] && exit 0
    serverName="${serverName//[[:space:]]/_}"
    [[ $? -ne 0 ]] && exit 0
    local serverAddress="$(inputbox "$(gettext "Enter the address of the server:")")"
    [[ $? -ne 0 ]] && exit 0
    local serverPort="${serverAddress##*:}"
    if ! [[ "$serverPort" =~ ^[0-9]+ ]]; then
        serverPort=64738
    fi
    mumbleServerList[$serverName]="${serverAddress}:${serverPort}"
    echo "declare -Ag mumbleServerList=(" > ~/.config/barnard/servers.conf
    for i in ${!mumbleServerList[@]} ; do
        echo "[${i}]=\"${mumbleServerList[$i]}\"" >> ~/.config/barnard/servers.conf
    done
    echo ")" >> ~/.config/barnard/servers.conf
    echo "Added server $serverName ${serverAddress}:${serverPort}" | log
    msgbox "$(gettext "Added server") $serverName"
    exit 0
}

connect() {
    declare -a serverList
    for i in ${!mumbleServerList[@]} ; do
        serverList+=("$i" "$i")
    done
    local serverName="$(menulist ${serverList[@]})"
    if [[ -z "$serverName" ]]; then
        exit 0
    fi
    command barnard -username ${USER}-f123 -server ${mumbleServerList[$serverName]} |& log
    exit 0
}

remove-server() {
    declare -a serverList
    for i in ${!mumbleServerList[@]} ; do
        serverList+=("$i" "$i")
    done
    local serverName="$(menulist ${serverList[@]})"
    if [[ -z "$serverName" ]]; then
        exit 0
    fi
    unset mumbleServerList[$serverName]
    echo "declare -Ag mumbleServerList=(" > ~/.config/barnard/servers.conf
    for i in ${!mumbleServerList[@]} ; do
        echo "[${i}]=\"${mumbleServerList[$i]}\"" >> ~/.config/barnard/servers.conf
    done
    echo ")" >> ~/.config/barnard/servers.conf
    echo "Removed server $serverName ${serverAddress}:${serverPort}" | log
    msgbox "$(gettext "Removed server") $serverName"
    exit 0
  }  

# Extract the function to be called
action="${0##*mumble-manager}"
action="${action/-/}"
action="${action%.*}"
if [[ "$action" == "" ]]; then
    action="connect"
fi

eval "$action"

exit 0
