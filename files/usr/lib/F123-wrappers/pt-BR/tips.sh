#!/bin/bash
# tips.sh
# Description
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
source /usr/lib/F123-includes/script_functions.sh
source /usr/lib/F123-includes/preferences-manager.sh
source ~/.preferences

# Create the directory structure if it doesn't exist.
[[ -d ~/.config/F123 ]] || mkdir -p ~/.config/F123

# Load or create the current tipIndex.
if [[ -r ~/.config/F123/.${1}.tipindex ]]; then
	tipIndex=$(<~/.config/F123/.${1}.tipindex)
else
	tipIndex=0
fi

# Tips are on by default, so set the preference if it's not found.
if [[ -z "${preferences[tips]}" ]]; then
    preferences[tips]=1
    write_preferences
fi

# Get the path where tips are stored for the current locale and application name ($1).
tipPath="/usr/share/doc/F123/tips/${LANG}/${1}.txt"

if [[ ! -r "$tipPath" ]]; then
	exit 0
fi

if (( ${preferences[tips]} )); then
	mapfile -t tips < ${tipPath}
	if  [[ ${tipIndex} -ge ${#tips[@]} ]]; then
		tipIndex=0
	fi
	echo "command say ${tips[$tipIndex]}" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
	((tipIndex++))
	echo -n "$tipIndex" > ~/.config/F123/.${1}.tipindex
fi

exit 0
