��          L      |       �      �   �   �   �   a  6    k   H  {  �  (   0  �   Y  �   �  +  �  u   �                                         Checking internet connection... Do you want to change passwords for users of this system? Since everyone knows that the default password is f123, it is recommended that you change it. Do you want to change security options for this system? If you press 'Enter' for yes, you will be able to make this computer a bit more secure, but also a bit less convenient. In order to use the internet, you need to set up a network connection.
	If you already plugged in a wire that is connected directly to a router or modem, you don't need to do anything here.
	However, if you don't have a wire connected, but want to use the internet, you can configure the wireless network here. Would you like to configure the wireless network now? An internet connection is required to continue setup. Project-Id-Version: files-F 123Light
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-31 23:12-0500
PO-Revision-Date: 2019-01-31 23:12-0500
Last-Translator: Cleverson Casarin Uliana <clul@mm.st>Language-Team: Brazilian Portuguese
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Verificando conecção com a Internet... Gostaria de alterar a senha para usuários deste sistema? Como todo mundo sabe que a senha padrão é f123, é recomendado que você a altere. Deseja alterar as opções de segurança deste sistema? Caso pressione Enter para alterar, poderá tornar este computador um pouco mais seguro, mas também um pouco menos conveniente. Para usar a Internet, você tem que configurar uma conecção de rede.
	Caso você já tenha plugado um fio que esteja conectado diretamente a um roteador ou modem, não precisa fazer nada aqui.
	Porém, se não tiver um fio conectado mas deseja usar a Internet, pode configurar aqui a rede sem fio. Gostaria de configurar a rede sem fio agora? É necessária uma conecção à Internet para continuar a instalação. 