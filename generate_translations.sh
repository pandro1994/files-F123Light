#!/bin/bash
# generate_translations.sh
# Description: A small script to create gettext translation files .pot, .po for files in files-F123Light
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

# this script must be ran inside files-F123light/
# get a list of files to translate.
fileList=($(find files/usr/lib/F123-wrappers -type f))

# Weed out the files that do not mention gettext
for i in "${!fileList[@]}" ; do
if ! grep -q "gettext" "${fileList[i]}" ; then
unset "fileList[i]"
fi
done

for i in ${fileList[@]} ; do
# Get the directory name for each file.
outFile="po/${i##*/}"
outFile="${outFile%.*}"
# Generate the .pot file.
mkdir -p "${outFile}"
xgettext -o "${outFile}/${i##*/}.pot" -d . -L shell "$i" "files/usr/lib/F123-includes/script_functions.sh"
done

exit 0 
