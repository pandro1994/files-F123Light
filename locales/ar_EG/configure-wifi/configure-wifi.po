msgid ""
msgstr ""
"Project-Id-Version: f123-files\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-29 17:13-0400\n"
"PO-Revision-Date: 2019-07-29 21:59\n"
"Last-Translator: Fernando Botelho (FernandoBotelho)\n"
"Language-Team: Arabic, Egypt\n"
"Language: ar_EG\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: f123-files\n"
"X-Crowdin-Language: ar-EG\n"
"X-Crowdin-File: /master/po/configure-wifi/configure-wifi.pot\n"

#: configure-wifi:45
msgid "Enter text and press enter. Press escape to cancel."
msgstr ""

#: configure-wifi:71
msgid "Use the up and down arrow keys to find the option you want, then press enter to select it. Press escape to cancel."
msgstr ""

#: configure-wifi:81
msgid "Please select your network device"
msgstr ""

#: configure-wifi:85
msgid "No wifi interfaces were found. Be sure that you have a wireless adapter securely connected to your machine."
msgstr ""

#: configure-wifi:96
msgid "Scanning for available wireless networks ..."
msgstr ""

#: configure-wifi:103
msgid "Please select your wireless network"
msgstr ""

#: configure-wifi:108
msgid "Unable to get a list of networks. Either you have no wireless networks in range or there is a problem with your wireless adapter."
msgstr ""

#: configure-wifi:112
#, sh-format
msgid "Enter the password for ${network}. Just press the enter key if $network is unsecured."
msgstr ""

#: configure-wifi:113
#, sh-format
msgid "Connecting to ${network}..."
msgstr ""

#: configure-wifi:184
msgid "Your wireless network connection has been configured and is now working."
msgstr ""

#: configure-wifi:185
msgid "There was a problem connecting to the wireless network you selected. Press enter to try another network, or press escape to exit."
msgstr ""

