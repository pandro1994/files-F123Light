# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-18 09:32-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: barnard-ui:51 barnard-ui:59
msgid "Enter text and press enter."
msgstr ""

#: barnard-ui:74
msgid "Press 'Enter' for \"yes\" or 'Escape' for \"no\"."
msgstr ""

#: barnard-ui:90
msgid ""
"Use the up and down arrow keys to find the option you want, then press enter "
"to select it."
msgstr ""

#: barnard-ui:93
msgid "Please select one"
msgstr ""

#: barnard-ui:106
msgid "Enter a name for the new server:"
msgstr ""

#: barnard-ui:108
msgid "Enter the address of the server:"
msgstr ""

#: barnard-ui:121
msgid "Added server"
msgstr ""

#: barnard-ui:153
msgid "Removed server"
msgstr ""
