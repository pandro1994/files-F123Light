# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-31 23:41-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../scripts/update-f123light:54
msgid ""
"Your system has been updated. We recommend that you restart your computer. "
"Please press 'Enter' to restart or 'Escape' to finish the update without "
"restarting."
msgstr ""

#: ../../scripts/update-f123light:85
msgid "Usage"
msgstr ""

#: ../../scripts/update-f123light:86
#, sh-format
msgid "$name [options...]"
msgstr ""

#: ../../scripts/update-f123light:87
msgid "Options"
msgstr ""

#: ../../scripts/update-f123light:88
msgid "Print this help message and exit"
msgstr ""

#: ../../scripts/update-f123light:89
#, sh-format
msgid "Get files from the specified git branch, default: $branch"
msgstr ""

#: ../../scripts/update-f123light:97
msgid "not recognized"
msgstr ""

#: ../../scripts/update-f123light:107
msgid ""
"Your system needs to restart before updates can begin. Would you like to do "
"this now?"
msgstr ""

#: ../../scripts/update-f123light:117
msgid ""
"Does not currently have administrator access to this system. Please upgrade "
"with a user that has adminstrator access."
msgstr ""

#: ../../scripts/update-f123light:122
msgid "Checking internet connection..."
msgstr ""

#: ../../scripts/update-f123light:126
msgid ""
"The software update process for F123Light will now begin. Depending on how "
"many updates there are, your password might be requested more than once. Do "
"not be alarmed, as this is normal. Press the 'Enter' key to continue."
msgstr ""

#: ../../scripts/update-f123light:132
msgid "Updating system software..."
msgstr ""

#: ../../scripts/update-f123light:135
msgid "Updating configuration and other files..."
msgstr ""

#: ../../scripts/update-f123light:158
msgid "Performing incremental updates..."
msgstr ""

#: ../../scripts/update-f123light:167
msgid "Update complete."
msgstr ""
